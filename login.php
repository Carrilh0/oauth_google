<html>
    <head>
    <title>LOGIN</title>
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <meta name="google-signin-client_id" content="880909752363-52b1hd2d07m1p528nnu6vqoig2q3uqlk.apps.googleusercontent.com">
    </head>
    
    <body>

    <div class="g-signin2" data-onsuccess="onSignIn"></div>

    <p id="msg"></p>

    <script>
    function onSignIn(googleUser) {
        var profile = googleUser.getBasicProfile();
        var userId = profile.getId();
        var userName = profile.getName();
        var userImageUrl = profile.getImageUrl();
        var userEmail = profile.getEmail(); // This is null if the 'email' scope is not present.
    
        var userToken = googleUser.getAuthResponse().id_token;

        if (userName !== ''){
            var dados = {
                userId:userId,
                userName:userName,
                userImageUrl:userImageUrl,
                userEmail:userEmail
            };
                $.post('valida.php',dados,function(retorna){
                    if (retorna === '"erro"'){
                        var msg = 'Usuario não encontrado com esse email';
                        document.getElementById('msg').innerHTML = msg;
                    }else{
                        window.location.href = retorna;
                    }
                    
                });
        
        }else{
            var msg = "Usuario não encontrado"
            document.getElementById('msg').innerHTML = msg;
        }
        
    }
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    </body>
</html>